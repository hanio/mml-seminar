import torch
from torch import nn
import torch.nn.functional as F
import torch.optim as optim
import h5py

def h5_to_tensor(filename, data_path='Data/'):
    h5_file = h5py.File(data_path + filename, 'r')['d1']
    tensor = torch.as_tensor(h5_file, dtype=torch.float)
    return tensor
    

audio_train = h5_to_tensor('audio_train.h5')#(15290,20,74)
audio_valid = h5_to_tensor('audio_valid.h5')#(2291,20,74)
video_train = h5_to_tensor('video_train.h5')#(15290,20,35)
video_valid = h5_to_tensor('video_valid.h5')#(2291,20,35)
text_train = h5_to_tensor('text_train_emb.h5')#(15290,20,300)
text_valid = h5_to_tensor('text_valid_emb.h5')#(2291,20,300)

audio_train = torch.cat((audio_train,audio_valid))
video_train = torch.cat((video_train,video_valid))
text_train = torch.cat((text_train,text_valid))

ey_train = h5_to_tensor('ey_train.h5')#(15290,6)
ey_valid = h5_to_tensor('ey_valid.h5')#(2291,6)
ey_test = h5_to_tensor('ey_test.h5')#(4832,6)

ey_train = torch.cat((ey_train,ey_valid))

CONFIGURATIONS = [(m,s,d) for m in ['WITHOUT','ONLY'] 
        for s in ['AUDIO','VIDEO','TEXT'] 
        for d in ['NO_SIGNAL','NOISE']]
CONFIGURATIONS.insert(0,('ALL',None,None))

TRAIN = ey_train.size()[0]

class Emo_LSTM(nn.Module):

    def __init__(self):
        super(Emo_LSTM, self).__init__()

        hidden_dim=600
        self.lstm_audio = nn.LSTM(1480, hidden_dim)
        self.lstm_video = nn.LSTM(700, hidden_dim)
        self.lstm_text = nn.LSTM(6000, hidden_dim)
        self.hidden2emo = nn.Linear(hidden_dim,6)


    def forward(self, audio_in, video_in, text_in):
        audio_in=torch.reshape(audio_in, (1,1,-1))
        video_in=torch.reshape(video_in, (1,1,-1))
        text_in=torch.reshape(text_in, (1,1,-1))
        
        lstm_audio_out, _ = self.lstm_audio(audio_in)
        lstm_video_out, _ = self.lstm_video(video_in)
        lstm_text_out, _ = self.lstm_text(text_in)
        
        out = lstm_audio_out[-1]+ lstm_video_out[-1]+ lstm_text_out[-1]
        
        out = self.hidden2emo(out)
        out = F.relu(out)
        emo_score = F.softmax(out, dim=1)
        return emo_score

net = Emo_LSTM()

criterion = nn.BCEWithLogitsLoss()
optimizer = optim.SGD(net.parameters(), lr=0.1)

# Training
for epoch in range(1,31):

    for i ,labels in enumerate(ey_train):
        labels = labels.unsqueeze(0)

        optimizer.zero_grad()

        outputs = net(audio_train[i], video_train[i], text_train[i])
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        
        if i%int(TRAIN/100)==0: print('epoch',epoch,int(100*i/TRAIN),'% ',end='\r')

# Results
def dysfunction(defect, source):
    if defect == 'NO_SIGNAL':
        return torch.zeros(*source.shape)
    elif defect == 'NOICE':
        return torch.randn(*source.shape)
    else:
        return source

def load_testsets(mode, selected_source, defect):
    audio_test = h5_to_tensor('audio_test.h5')#(4832,20,74)
    video_test = h5_to_tensor('video_test.h5')#(4832,20,35)
    text_test = h5_to_tensor('text_test_emb.h5')#(4832,20,300)

    if mode == 'WITHOUT' and selected_source == 'AUDIO':
        audio_test = dysfunction(defect, audio_test)
    if mode == 'WITHOUT' and selected_source == 'VIDEO':
        video_test = dysfunction(defect, video_test)
    if mode == 'WITHOUT' and selected_source == 'TEXT':
        text_test = dysfunction(defect, text_test)
    if mode == 'ONLY' and selected_source == 'AUDIO':
        video_test = dysfunction(defect, video_test)
        text_test = dysfunction(defect, text_test)
    if mode == 'ONLY' and selected_source == 'VIDEO':
        audio_test = dysfunction(defect, audio_test)
        text_test = dysfunction(defect, text_test)
    if mode == 'ONLY' and selected_source == 'TEXT':
        audio_test = dysfunction(defect, audio_test)
        video_test = dysfunction(defect, video_test)
    return (audio_test, video_test, text_test)


for mode, selected_source, defect in CONFIGURATIONS:
    
    audio_test, video_test, text_test = load_testsets(mode, selected_source, defect)
    correct = 0
    total = 0
    p = []
    cp = []
    maxes = []
    with torch.no_grad():
        for i, labels in enumerate(ey_test):
            labels = labels.unsqueeze(0)
            outputs = net(audio_test[i], video_test[i], text_test[i])
            _, predicted = torch.max(outputs.data, 1)
            m, truth = torch.max(labels.data, 1)
            maxes = labels.data[0]>=m
            total += labels.size(0)
            correct += maxes[predicted.item()]
            p.append(predicted[0].item())
            if maxes[predicted.item()]:
                cp.append(predicted[0].item())
    from collections import Counter
    print(mode, selected_source, defect)
    print('predicted:',Counter(p))
    print('correctly predicted:',Counter(cp))
    print('Accuracy: %d%%'%(100*correct/total))
    print()
